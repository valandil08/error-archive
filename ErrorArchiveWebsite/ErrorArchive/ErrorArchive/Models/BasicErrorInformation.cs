﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ErrorArchive.Models
{
    public class BasicErrorInformation
    {
        public DataTable errors { get; set; }
    }
}