﻿using ErrorArchive.Models;
using DevLab.Core.Connections.SQL;
using System.Web.Mvc;
using System.Data;
using System;

namespace ErrorArchive.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            BasicErrorInformation model = new BasicErrorInformation();

            SqlServerDb db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("[dbo].[uspSearchBasicErrors]");
            model.errors = db.RunDataTableRequest();

            return View(model);
        }

        public ActionResult ViewErrorDetails(int id = -1)
        {
            if (id == -1)
            {
                return RedirectToAction("Index", "Home");
            }

            SqlServerDb db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("[dbo].[uspCheckErrorIdValid]");
            db.AddParameter<int>("ErrorId", id);
            int? value = (int?)db.RunScalarRequest();

            if (value != 1)
            {
                return RedirectToAction("Index", "Home");
            }
            FullErrorInformationModel model = new FullErrorInformationModel();

            db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("[dbo].[uspGetBasicErrorInformation]");
            db.AddParameter<int>("ErrorId", id);
            DataTable basicInfo = db.RunDataTableRequest();
            model.BasicInfo = basicInfo.Rows[0];

            if (model.BasicInfo["MethodDataId"] != DBNull.Value)
            {
                db = new SqlServerDb("ErrorArchive");
                db.SetStoredProcedure("[dbo].[uspGetMethodErrorInformation]");
                db.AddParameter<string>("MethodDataId", model.BasicInfo["MethodDataId"].ToString());
                DataTable methodInfo = db.RunDataTableRequest();
                model.MethodInfo = methodInfo.Rows[0];
            }


            if (model.BasicInfo["WebDataId"] != DBNull.Value)
            {
                db = new SqlServerDb("ErrorArchive");
                db.SetStoredProcedure("[dbo].[uspGetWebErrorInformation]");
                db.AddParameter<string>("WebDataId", model.BasicInfo["WebDataId"].ToString());
                DataTable webInfo = db.RunDataTableRequest();
                model.WebInfo = webInfo.Rows[0];
            }

            return View(model);
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}