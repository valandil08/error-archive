USE [master]
GO
CREATE LOGIN [ErrorArchiveUser] WITH PASSWORD=N'averybadpassword1234', DEFAULT_DATABASE=[ErrorArchive], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
USE [ErrorArchive]
GO
CREATE USER [ErrorArchiveUser] FOR LOGIN [ErrorArchiveUser]
GO
USE [ErrorArchive]
GO
ALTER ROLE [db_owner] ADD MEMBER [ErrorArchiveUser]
GO
