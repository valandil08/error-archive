/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2014 (12.0.2000)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [ErrorArchive]
GO
/****** Object:  User [ErrorArchiveUser]    Script Date: 05/09/2018 19:12:43 ******/
CREATE USER [ErrorArchiveUser] FOR LOGIN [ErrorArchiveUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [ErrorArchiveUser]
GO
/****** Object:  Schema [Update]    Script Date: 05/09/2018 19:12:43 ******/
CREATE SCHEMA [Update]
GO
/****** Object:  Table [dbo].[tbActiveUsers]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbActiveUsers](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ActiveUsers] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbActiveUsers] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbClasses]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbClasses](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbClasses] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbErrorMessages]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbErrorMessages](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NOT NULL,
 CONSTRAINT [PK_ErrorMessage] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbErrorMessages] UNIQUE NONCLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbErrors]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbErrors](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[SystemId] [int] NOT NULL,
	[TechnologyId] [int] NOT NULL,
	[ErrorTypeId] [int] NOT NULL,
	[ObjectThrownInId] [int] NOT NULL,
	[ErrorMessageId] [int] NOT NULL,
	[MachineNameId] [int] NULL,
	[ActiveUserId] [int] NULL,
	[MethodDataId] [int] NULL,
	[WebDataId] [int] NULL,
	[SqlDataId] [int] NULL,
	[DateTimeAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Main] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbErrorTypes]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbErrorTypes](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ErrorType] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbErrorTypes] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbMachineNames]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbMachineNames](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_MachineNames] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbMachineNames] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbMethodData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbMethodData](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NULL,
	[ClassId] [int] NULL,
	[MethodParametersId] [int] NULL,
	[StackTraceId] [int] NULL,
 CONSTRAINT [PK_MethodLinker] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbMethodParameters]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbMethodParameters](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Parameters] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tbMethodParameters] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbObjectThrownIn]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbObjectThrownIn](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ObjectThrownIn] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbPackages]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPackages](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Packages] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbPackages] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbSqlData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSqlData](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[DatabaseId] [int] NOT NULL,
	[Severity] [int] NOT NULL,
	[LineNumber] [int] NOT NULL,
 CONSTRAINT [PK_SqlInfoLinker] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbSqlDatabases]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSqlDatabases](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_SqlDatabases] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbSqlDatabases] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbStackTraces]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbStackTraces](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tbStackTraces] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbSystems]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSystems](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tbSystems] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbTechnologies]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbTechnologies](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NOT NULL,
 CONSTRAINT [PK_tbTechnologies] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbTechnologies] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbWebData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbWebData](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[UrlId] [int] NOT NULL,
	[QueryStringId] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
 CONSTRAINT [PK_WebInfoLinker] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbWebQueryStrings]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbWebQueryStrings](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_tbWebQueryStrings] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbWebQueryStrings] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbWebSessions]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbWebSessions](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_WebSessions] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbWebSessions] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbWebUrls]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbWebUrls](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_WebUrls] PRIMARY KEY CLUSTERED 
(
	[ROWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_tbWebUrls] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwActiveUsers]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwActiveUsers]
AS
SELECT        ROWID, Name
FROM            dbo.tbActiveUsers
GO
/****** Object:  View [dbo].[vwErrorMessages]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwErrorMessages]
AS
SELECT        ROWID, Message
FROM            dbo.tbErrorMessages
GO
/****** Object:  View [dbo].[vwErrors]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vwErrors]
AS
SELECT        ROWID, SystemId, TechnologyId, ErrorTypeId, ObjectThrownInId, ErrorMessageId, MachineNameId, ActiveUserId, MethodDataId, WebDataId, SqlDataId, DateTimeAdded
FROM            dbo.tbErrors
GO
/****** Object:  View [dbo].[vwErrorTypes]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwErrorTypes]
AS
SELECT        ROWID, Name
FROM            dbo.tbErrorTypes
GO
/****** Object:  View [dbo].[vwMachineNames]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwMachineNames]
AS
SELECT        ROWID, Name
FROM            dbo.tbMachineNames
GO
/****** Object:  View [dbo].[vwObjectThrownIn]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwObjectThrownIn]
AS
SELECT        ROWID, Name
FROM            dbo.tbObjectThrownIn
GO
/****** Object:  View [dbo].[vwSystems]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwSystems]
AS
SELECT        ROWID, Name
FROM            dbo.tbSystems
GO
/****** Object:  View [dbo].[vwTechnologies]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwTechnologies]
AS
SELECT        ROWID, Name
FROM            dbo.tbTechnologies
GO
/****** Object:  View [dbo].[vwBasicErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwBasicErrorInformation]
AS
SELECT        dbo.vwErrors.ROWID, dbo.vwSystems.Name AS System, dbo.vwTechnologies.Name AS Technology, dbo.vwErrorTypes.Name AS ErrorType, dbo.vwObjectThrownIn.Name AS ObjectThrownIN, 
                         dbo.vwErrorMessages.Message AS ErrorMessage, dbo.vwMachineNames.Name AS MachineName, dbo.vwActiveUsers.Name AS ActiveUser, dbo.vwErrors.MethodDataId, dbo.vwErrors.WebDataId, dbo.vwErrors.SqlDataId, 
                         dbo.vwErrors.DateTimeAdded
FROM            dbo.vwErrors LEFT OUTER JOIN
                         dbo.vwSystems ON dbo.vwErrors.SystemId = dbo.vwSystems.ROWID LEFT OUTER JOIN
                         dbo.vwTechnologies ON dbo.vwErrors.TechnologyId = dbo.vwTechnologies.ROWID LEFT OUTER JOIN
                         dbo.vwErrorTypes ON dbo.vwErrors.ErrorTypeId = dbo.vwErrorTypes.ROWID LEFT OUTER JOIN
                         dbo.vwObjectThrownIn ON dbo.vwErrors.ObjectThrownInId = dbo.vwObjectThrownIn.ROWID LEFT OUTER JOIN
                         dbo.vwErrorMessages ON dbo.vwErrors.ErrorMessageId = dbo.vwErrorMessages.ROWID LEFT OUTER JOIN
                         dbo.vwMachineNames ON dbo.vwErrors.MachineNameId = dbo.vwMachineNames.ROWID LEFT OUTER JOIN
                         dbo.vwActiveUsers ON dbo.vwErrors.ActiveUserId = dbo.vwActiveUsers.ROWID
GO
/****** Object:  View [dbo].[vwClasses]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwClasses]
AS
SELECT        ROWID, Name
FROM            dbo.tbClasses
GO
/****** Object:  View [dbo].[vwMethodData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vwMethodData]
AS
SELECT        ROWID, PackageId, ClassId, MethodParametersId, StackTraceId
FROM            dbo.tbMethodData
GO
/****** Object:  View [dbo].[vwMethodParameters]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwMethodParameters]
AS
SELECT        ROWID, Parameters
FROM            dbo.tbMethodParameters
GO
/****** Object:  View [dbo].[vwPackages]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwPackages]
AS
SELECT        ROWID, Name
FROM            dbo.tbPackages
GO
/****** Object:  View [dbo].[vwStackTraces]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vwStackTraces]
AS
SELECT        ROWID, Value
FROM            dbo.tbStackTraces
GO
/****** Object:  View [dbo].[vwMethodErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwMethodErrorInformation]
AS
SELECT        dbo.vwMethodData.ROWID, dbo.vwClasses.Name AS Class, dbo.vwMethodParameters.Parameters AS MethodParameters, dbo.vwPackages.Name AS Package, dbo.vwStackTraces.Value AS StackTrace, 
                         dbo.vwMethodData.PackageId, dbo.vwMethodData.ClassId, dbo.vwMethodData.MethodParametersId, dbo.vwMethodData.StackTraceId
FROM            dbo.vwMethodData LEFT OUTER JOIN
                         dbo.vwPackages ON dbo.vwMethodData.PackageId = dbo.vwPackages.ROWID LEFT OUTER JOIN
                         dbo.vwStackTraces ON dbo.vwMethodData.StackTraceId = dbo.vwStackTraces.ROWID LEFT OUTER JOIN
                         dbo.vwMethodParameters ON dbo.vwMethodData.MethodParametersId = dbo.vwMethodParameters.ROWID LEFT OUTER JOIN
                         dbo.vwClasses ON dbo.vwMethodData.ClassId = dbo.vwClasses.ROWID
GO
/****** Object:  View [dbo].[vwWebData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vwWebData]
AS
SELECT        ROWID, UrlId, QueryStringId, SessionId
FROM            dbo.tbWebData
GO
/****** Object:  View [dbo].[vwWebQueryStrings]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwWebQueryStrings]
AS
SELECT        ROWID, Name
FROM            dbo.tbWebQueryStrings
GO
/****** Object:  View [dbo].[vwWebSessions]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwWebSessions]
AS
SELECT        ROWID, Name
FROM            dbo.tbWebSessions
GO
/****** Object:  View [dbo].[vwWebUrls]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwWebUrls]
AS
SELECT        ROWID, Name
FROM            dbo.tbWebUrls
GO
/****** Object:  View [dbo].[vwWebErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwWebErrorInformation]
AS
SELECT        dbo.vwWebData.ROWID, dbo.vwWebSessions.Name AS Session, dbo.vwWebUrls.Name AS URL, dbo.vwWebQueryStrings.Name AS QueryString
FROM            dbo.vwWebData LEFT OUTER JOIN
                         dbo.vwWebQueryStrings ON dbo.vwWebData.QueryStringId = dbo.vwWebQueryStrings.ROWID LEFT OUTER JOIN
                         dbo.vwWebUrls ON dbo.vwWebData.UrlId = dbo.vwWebUrls.ROWID LEFT OUTER JOIN
                         dbo.vwWebSessions ON dbo.vwWebData.SessionId = dbo.vwWebSessions.ROWID
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SearchBasicErrors]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_SearchBasicErrors]
(	
	@System			varchar(max) = NULL,
	@Technology		varchar(max) = NULL,
	@ErrorType		varchar(max) = NULL,
	@ObjectThrownIn varchar(max) = NULL,
	@MachineName	varchar(max) = NULL,
	@ActiveUser		varchar(max) = NULL,
	@ErrorMessage	varchar(max) = NULL,
	@MinDateTime	datetime	 = NULL,
	@MaxDateTime	datetime	 = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT      
	dbo.vwErrors.ROWID, 
	dbo.vwSystems.Name AS System, 
	dbo.vwTechnologies.Name AS Technology, 
	dbo.vwErrorTypes.Name AS ErrorType, 
	dbo.vwObjectThrownIn.Name AS ObjectThrownIn, 
	dbo.vwMachineNames.Name AS MachineName, 
	dbo.vwActiveUsers.Name AS ActiveUser, 
	dbo.vwErrorMessages.Message AS ErrorMessage,
    dbo.vwErrors.MethodDataId,
    dbo.vwErrors.WebDataId,
    dbo.vwErrors.SqlDataId, 
	dbo.vwErrors.DateTimeAdded As DateTimeAdded	
	FROM            
	dbo.vwErrors 

	INNER JOIN dbo.vwErrorTypes  ON 
	dbo.vwErrorTypes.ROWID = dbo.vwErrors.ErrorTypeId

	LEFT JOIN dbo.vwActiveUsers ON
	dbo.vwErrors.ActiveUserId = dbo.vwActiveUsers.ROWID 

	LEFT JOIN dbo.vwMachineNames ON 
	dbo.vwErrors.MachineNameId = dbo.vwMachineNames.ROWID

	INNER JOIN dbo.vwObjectThrownIn ON 
	dbo.vwErrors.ObjectThrownInId = dbo.vwObjectThrownIn.ROWID 

	INNER JOIN dbo.vwSystems ON 
	dbo.vwErrors.SystemId = dbo.vwSystems.ROWID 

	INNER JOIN dbo.vwTechnologies ON 
	dbo.vwErrors.TechnologyId = dbo.vwTechnologies.ROWID 

	INNER JOIN dbo.vwErrorMessages ON 
	dbo.vwErrors.ErrorMessageId = dbo.vwErrorMessages.ROWID

	WHERE 
	vwErrorTypes.Name		  =  COALESCE(@ErrorType,vwErrorTypes.Name) AND
	vwTechnologies.Name		  >= COALESCE(@Technology,vwTechnologies.Name) AND
	vwSystems.Name			  <= COALESCE(@System,vwSystems.Name)AND
	vwErrorMessages.Message	  =	 COALESCE(@ErrorMessage,vwErrorMessages.Message) AND
	vwObjectThrownIn.Name	  =	 COALESCE(@ObjectThrownIn,vwObjectThrownIn.Name) AND
	(
		vwMachineNames.Name	  =	 COALESCE(@MachineName,vwMachineNames.Name) OR
		vwMachineNames.Name	  IS NULL
	) 
	AND
	(
		vwActiveUsers.Name	  =	 COALESCE(@ActiveUser,vwActiveUsers.Name) OR
		vwActiveUsers.Name	  IS NULL
	)
	AND
	vwErrors.DateTimeAdded	  >= COALESCE(@MinDateTime,'01/01/1900 00:00:00') AND
	vwErrors.DateTimeAdded	  <= COALESCE(@MaxDateTime,GETDATE()) 
)
GO
/****** Object:  View [dbo].[vwSqlData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vwSqlData]
AS
SELECT        ROWID, DatabaseId, Severity, LineNumber
FROM            dbo.tbSqlData
GO
/****** Object:  View [dbo].[vwSqlDatabases]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwSqlDatabases]
AS
SELECT        ROWID, Name
FROM            dbo.tbSqlDatabases
GO
ALTER TABLE [dbo].[tbErrors] ADD  CONSTRAINT [DF_tbErrors_DateTimeAdded]  DEFAULT (getdate()) FOR [DateTimeAdded]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbActiveUsers] FOREIGN KEY([ActiveUserId])
REFERENCES [dbo].[tbActiveUsers] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbActiveUsers]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbErrorMessages] FOREIGN KEY([ErrorMessageId])
REFERENCES [dbo].[tbErrorMessages] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbErrorMessages]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbErrorTypes] FOREIGN KEY([ErrorTypeId])
REFERENCES [dbo].[tbErrorTypes] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbErrorTypes]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbMachineNames] FOREIGN KEY([MachineNameId])
REFERENCES [dbo].[tbMachineNames] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbMachineNames]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbMethodData] FOREIGN KEY([MethodDataId])
REFERENCES [dbo].[tbMethodData] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbMethodData]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbObjectThrownIn] FOREIGN KEY([ObjectThrownInId])
REFERENCES [dbo].[tbObjectThrownIn] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbObjectThrownIn]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbSqlData] FOREIGN KEY([SqlDataId])
REFERENCES [dbo].[tbSqlData] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbSqlData]
GO
ALTER TABLE [dbo].[tbErrors]  WITH CHECK ADD  CONSTRAINT [FK_tbErrors_tbSystems] FOREIGN KEY([SystemId])
REFERENCES [dbo].[tbSystems] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbSystems]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbTechnologies] FOREIGN KEY([TechnologyId])
REFERENCES [dbo].[tbTechnologies] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbTechnologies]
GO
ALTER TABLE [dbo].[tbErrors]  WITH NOCHECK ADD  CONSTRAINT [FK_tbErrors_tbWebData] FOREIGN KEY([WebDataId])
REFERENCES [dbo].[tbWebData] ([ROWID])
GO
ALTER TABLE [dbo].[tbErrors] CHECK CONSTRAINT [FK_tbErrors_tbWebData]
GO
ALTER TABLE [dbo].[tbSqlData]  WITH NOCHECK ADD  CONSTRAINT [FK_tbSqlInfoLinker_tbSqlDatabases] FOREIGN KEY([DatabaseId])
REFERENCES [dbo].[tbSqlDatabases] ([ROWID])
GO
ALTER TABLE [dbo].[tbSqlData] CHECK CONSTRAINT [FK_tbSqlInfoLinker_tbSqlDatabases]
GO
ALTER TABLE [dbo].[tbWebData]  WITH NOCHECK ADD  CONSTRAINT [FK_tbWebInfoLinker_tbWebQueryStrings] FOREIGN KEY([QueryStringId])
REFERENCES [dbo].[tbWebQueryStrings] ([ROWID])
GO
ALTER TABLE [dbo].[tbWebData] CHECK CONSTRAINT [FK_tbWebInfoLinker_tbWebQueryStrings]
GO
ALTER TABLE [dbo].[tbWebData]  WITH NOCHECK ADD  CONSTRAINT [FK_tbWebInfoLinker_tbWebSessions] FOREIGN KEY([SessionId])
REFERENCES [dbo].[tbWebSessions] ([ROWID])
GO
ALTER TABLE [dbo].[tbWebData] CHECK CONSTRAINT [FK_tbWebInfoLinker_tbWebSessions]
GO
ALTER TABLE [dbo].[tbWebData]  WITH NOCHECK ADD  CONSTRAINT [FK_tbWebInfoLinker_tbWebUrls] FOREIGN KEY([UrlId])
REFERENCES [dbo].[tbWebUrls] ([ROWID])
GO
ALTER TABLE [dbo].[tbWebData] CHECK CONSTRAINT [FK_tbWebInfoLinker_tbWebUrls]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddActiveUser]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddActiveUser]
@ActiveUser		varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddActiveUser
	
		INSERT INTO [dbo].[vwActiveUsers]
		([Name])
		VALUES
		(@ActiveUser)			
	
		COMMIT TRANSACTION AddActiveUser

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddActiveUser

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddClass]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddClass]
@Class	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddClass
	
		INSERT INTO [dbo].[vwClasses]
			   ([Name])
		 VALUES
           (@Class)		
	
		COMMIT TRANSACTION AddClass

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddClass

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddErrorMessage]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddErrorMessage]
@ErrorMessage		varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRANSACTION AddErrorMessage

		INSERT INTO [dbo].[vwErrorMessages]
		([Message])
		VALUES
		(@ErrorMessage)	
	
		COMMIT TRANSACTION AddErrorMessage

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddErrorMessage

		-- Log Error
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddErrorType]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddErrorType]
@ErrorType			varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	
	
	BEGIN TRY
		BEGIN TRANSACTION AddErrorType	

		INSERT INTO [dbo].[vwErrorTypes]
		([Name])
		VALUES
		(@ErrorType)	
	
		COMMIT TRANSACTION AddErrorType

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddErrorType

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddMachineName]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddMachineName]
@MachineName		varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	
	
	BEGIN TRY
		BEGIN TRANSACTION AddMachineName	

		INSERT INTO [dbo].[vwMachineNames]
		([Name])
		VALUES
		(@MachineName)		
	
		COMMIT TRANSACTION AddMachineName

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddMachineName

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddMethodData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddMethodData]
@ClassId				int					= NULL,
@PackageId				int					= NULL,
@MethodParametersId		int					= NULL,
@StackTraceId			int					= NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRANSACTION AddObjectThrownIn

		INSERT INTO [dbo].[tbMethodData]			
           ([PackageId]
           ,[ClassId]
           ,[MethodParametersId]
           ,[StackTraceId])
		VALUES
           (@PackageId
           ,@ClassId
           ,@MethodParametersId
		   ,@StackTraceId)		
	
		COMMIT TRANSACTION AddObjectThrownIn

		RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddObjectThrownIn;

		THROW;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddMethodParameters]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddMethodParameters]
@MethodParameter	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddMethodParameter
	
		INSERT INTO [dbo].[vwMethodParameters]
			   ([Parameters])
		 VALUES
           (@MethodParameter)		
	
		COMMIT TRANSACTION AddActiveUser

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddMethodParameter

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddObjectThrownIn]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddObjectThrownIn]
@ObjectThrownIn		varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRANSACTION AddObjectThrownIn

		INSERT INTO [dbo].[vwObjectThrownIn]
		([Name])
		VALUES
		(@ObjectThrownIn)			
	
		COMMIT TRANSACTION AddObjectThrownIn

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddObjectThrownIn

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddPackage]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddPackage]
@Package	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddPackage
	
		INSERT INTO [dbo].[vwPackages]
			   ([Name])
		 VALUES
           (@Package)		
	
		COMMIT TRANSACTION AddActiveUser

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddPackage

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddQueryString]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddQueryString]
@QueryString	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddQueryString
	
		INSERT INTO [dbo].[vwWebQueryStrings]
			   ([Name])
		 VALUES
           (@QueryString)		
	
		COMMIT TRANSACTION AddQueryString

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddQueryString

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddSession]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddSession]
@Session	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddSession
	
		INSERT INTO [dbo].[vwWebSessions]
			   ([Name])
		 VALUES
           (@Session)		
	
		COMMIT TRANSACTION AddSession

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddSession

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddStackTrace]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddStackTrace]
@StackTrace	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddStackTrace
	
		INSERT INTO [dbo].[vwStackTraces]
			   ([Value])
		 VALUES
           (@StackTrace)		
	
		COMMIT TRANSACTION AddActiveUser

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddStackTrace

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddSystem]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddSystem]
@System	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddSystem
	
		INSERT INTO [dbo].[vwSystems]
			   ([Name])
		 VALUES
           (@System)		
	
		COMMIT TRANSACTION AddActiveUser

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddSystem

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddTechnology]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddTechnology]
@Technology	varchar(500)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddTechnoloy
	
		INSERT INTO [dbo].[vwTechnologies]
			   ([Name])
		 VALUES
           (@Technology)		
	
		COMMIT TRANSACTION AddTechnoloy

		RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddTechnoloy

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddURL]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddURL]
@URL	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;	

	BEGIN TRY
		BEGIN TRANSACTION AddURL
	
		INSERT INTO [dbo].[vwWebUrls]
			   ([Name])
		 VALUES
           (@URL)		
	
		COMMIT TRANSACTION AddURL

	RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddURL

		-- Log Error
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddWebData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AddWebData]
@UrlId					int					= NULL,
@QueryStringId			int					= NULL,
@SessionId				int					= NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRANSACTION AddObjectThrownIn

		INSERT INTO [dbo].[vwWebData]
           ([UrlId]
           ,[QueryStringId]
           ,[SessionId])
     VALUES
           (@UrlId,@QueryStringId,@SessionId)	
	
		COMMIT TRANSACTION AddObjectThrownIn

		RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION AddObjectThrownIn;

		THROW;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CheckErrorIdValid]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CheckErrorIdValid]
@ErrorId	int,
@InternalCall bit = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS 
	(
		SELECT 1
		FROM [dbo].[vwErrors]
		WHERE [ROWID] = @ErrorId
	)
	BEGIN
		SELECT 1;
		RETURN 1;
	END
	ELSE
	BEGIN
		SELECT 0;
		RETURN 0;
	END

END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetActiveUserId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetActiveUserId]
@ActiveUser					varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;	

	DECLARE @ActiveUserId INT = -1;

	IF (@InsertIfNotFound = 1)
	BEGIN	 

		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwActiveUsers]
			WHERE [vwActiveUsers].[Name] = @ActiveUser
		)
		BEGIN

			EXEC @ActiveUserId = [dbo].[usp_AddActiveUser] 
			@ActiveUser = @ActiveUser

		END
	END
	 
	IF (@ActiveUserId = -1)
	BEGIN

		SELECT  @ActiveUserId = [ROWID]
		FROM  [dbo].[vwActiveUsers]
		WHERE [vwActiveUsers].[Name] = @ActiveUser

	END

	RETURN @ActiveUserId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBasicErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBasicErrorInformation]
@ErrorId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	  [System]
      ,[Technology]
      ,[ErrorType]
      ,[ObjectThrownIN]
      ,[ErrorMessage]
      ,[MachineName]
      ,[ActiveUser]
      ,[MethodDataId]
      ,[WebDataId]
      ,[SqlDataId]
      ,[DateTimeAdded]
  FROM [dbo].[vwBasicErrorInformation]
  WHERE [ROWID] = @ErrorId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetChartData_DailyErrorCountOverLast30Days]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetChartData_DailyErrorCountOverLast30Days]
AS
BEGIN
	DECLARE @MinDate DATE = dateadd(DAY,0, datediff(day,0, getdate()));

	CREATE TABLE #Tally
	(
		DateAdded	Date,
		DayNum	int,
		Tally int
	);

	DECLARE @Counter int = 0;

	WHILE(@Counter < 30)
	BEGIN
		INSERT INTO #Tally
		([DateAdded], [DayNum], [Tally])
		VALUES
		(DATEADD(DAY,-1 * @Counter,@MinDate),(@Counter + 1),0)

		SET @Counter += 1;
	END

	UPDATE #Tally
	SET Tally = [Data].[Tally]
	FROM 
	(
		SELECT COUNT([ROWID]) AS [Tally],
		dateadd(DAY,0, datediff(day,0, [DateTimeAdded])) AS [DateAdded]
		FROM [ErrorArchive].[dbo].[tbErrors]
		WHERE [DateTimeAdded] >= DATEADD(DAY, -30,@MinDate)
		group by dateadd(DAY,0, datediff(day,0, [DateTimeAdded]))
	) as [Data]
	WHERE #Tally.[DateAdded] = [Data].[DateAdded]

	SELECT Tally as Tally
		,DayNum
		,DateAdded as [Date]
	FROM #Tally

	DROP TABLE #Tally;
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetClassId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetClassId]
@Class						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClassId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwClasses]
			WHERE [vwClasses].[Name] = @Class
		)
		BEGIN	
					 
			EXEC @ClassId = [dbo].[usp_AddClass] 
			@Class = @Class
					 
		END	

	END

	IF (@ClassId = -1)
	BEGIN

		SELECT  @ClassId = [ROWID] 
		FROM  [dbo].[vwClasses]
		WHERE [vwClasses].[Name] = @Class

	END

	RETURN @ClassId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetErrorMessageId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetErrorMessageId]
@ErrorMessage				varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;	

	DECLARE @ErrorMessageId INT = -1;

	IF (@InsertIfNotFound = 1)
	BEGIN

		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwErrorMessages]
			WHERE [vwErrorMessages].[Message] = @ErrorMessage
		)
		BEGIN	

			EXEC @ErrorMessageId = [dbo].[usp_AddErrorMessage] 
			@ErrorMessage = @ErrorMessage
					 
		END		
	END

	IF (@ErrorMessageId = -1)
	BEGIN
	 
		SELECT  @ErrorMessageId = [ROWID] 
		FROM  [dbo].[vwErrorMessages]
		WHERE [vwErrorMessages].[Message] = @ErrorMessage

	END

	RETURN @ErrorMessageId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetErrorTypeId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetErrorTypeId]
@ErrorType				varchar(max),
@InsertIfNotFound		bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ErrorTypeId INT = -1;

	IF (@InsertIfNotFound = 1)
	BEGIN	
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwErrorTypes]
			WHERE [vwErrorTypes].[Name] = @ErrorType
		)
		BEGIN

			EXEC [dbo].[usp_AddErrorType] 
			@ErrorType = @ErrorType

		END
	END

	IF (@ErrorTypeId = -1)
	BEGIN
	 
		SELECT  @ErrorTypeId = [ROWID] 
		FROM  [dbo].[vwErrorTypes]
		WHERE [vwErrorTypes].[Name] = @ErrorType

	END

	RETURN @ErrorTypeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMachineNameId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMachineNameId]
@MachineName				varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MachineNameId INT = -1;

	IF (@InsertIfNotFound = 1)
	BEGIN	

		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM [dbo].[vwMachineNames]
			WHERE [vwMachineNames].[Name] = @MachineName
		)
		BEGIN			
			 
			EXEC @MachineNameId = [dbo].[usp_AddMachineName] 
			@MachineName = @MachineName
					 
		END	
	END
	 
	IF (@MachineNameId = -1)
	BEGIN

		SELECT @MachineNameId = [ROWID] 
		FROM  [dbo].[vwMachineNames]
		WHERE [vwMachineNames].[Name] = @MachineName

	END

	RETURN @MachineNameId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMethodDataId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMethodDataId]
@ClassId				int					= NULL,
@PackageId				int					= NULL,
@MethodParametersId		int					= NULL,
@StackTraceId			int					= NULL,
@InsertIfNotFound		bit					= 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MethodDataId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT [ROWID] 
			FROM  [dbo].[vwMethodData]
			WHERE
			[vwMethodData].[ClassId] = @ClassId and
			[vwMethodData].[PackageId] = @PackageId and
			[vwMethodData].[MethodParametersId] = @MethodParametersId and
			[vwMethodData].[StackTraceId] = @StackTraceId
		)
		BEGIN						 
			EXEC @MethodDataId = [dbo].[usp_AddMethodData] 
			@ClassId = @ClassId,
			@PackageId = @PackageId,
			@MethodParametersId = @MethodParametersId,
			@StackTraceId = @StackTraceId
					 
		END	

	END

	IF (@MethodDataId = -1)
	BEGIN

		SELECT  @MethodDataId = [ROWID] 
		FROM  [dbo].[vwMethodData]
		WHERE
		[vwMethodData].[ClassId] = @ClassId and
		[vwMethodData].[PackageId] = @PackageId and
		[vwMethodData].[MethodParametersId] = @MethodParametersId and
		[vwMethodData].[StackTraceId] = @StackTraceId

	END

	RETURN @MethodDataId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMethodErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMethodErrorInformation]
@MethodDataId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	[Class]
    ,[MethodParameters]
    ,[Package]
    ,[StackTrace]
  FROM [dbo].[vwMethodErrorInformation]
  WHERE [ROWID] = @MethodDataId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMethodParametersId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMethodParametersId]
@MethodParameters						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MethodParameterId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwMethodParameters]
			WHERE [vwMethodParameters].[Parameters] = @MethodParameters
		)
		BEGIN	
					 
			EXEC @MethodParameterId = [dbo].[usp_AddMethodParameters] 
			@MethodParameter = @MethodParameters
					 
		END	

	END

	IF (@MethodParameterId = -1)
	BEGIN

		SELECT  @MethodParameterId = [ROWID] 
		FROM  [dbo].[vwMethodParameters]
		WHERE [vwMethodParameters].[Parameters] = @MethodParameters

	END

	RETURN @MethodParameterId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetObjectThrownInId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetObjectThrownInId]
@ObjectThrownIn				varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ObjectThrownInId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwObjectThrownIn]
			WHERE [vwObjectThrownIn].[Name] = @ObjectThrownIn
		)
		BEGIN	
					 
			EXEC @ObjectThrownInId = [dbo].[usp_AddObjectThrownIn] 
			@ObjectThrownIn = @ObjectThrownIn
					 
		END	

	END

	IF (@ObjectThrownInId = -1)
	BEGIN

		SELECT  @ObjectThrownInId = [ROWID] 
		FROM  [dbo].[vwObjectThrownIn]
		WHERE [vwObjectThrownIn].[Name] = @ObjectThrownIn

	END

	RETURN @ObjectThrownInId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPackageId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPackageId]
@Package						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PackageId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwPackages]
			WHERE [vwPackages].[Name] = @Package
		)
		BEGIN	
					 
			EXEC @PackageId = [dbo].[usp_AddPackage] 
			@Package = @Package
					 
		END	

	END

	IF (@PackageId = -1)
	BEGIN

		SELECT  @PackageId = [ROWID] 
		FROM  [dbo].[vwPackages]
		WHERE [vwPackages].[Name] = @Package

	END

	RETURN @PackageId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetQueryStringId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetQueryStringId]
@QueryString						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @QueryStringId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwWebQueryStrings]
			WHERE [vwWebQueryStrings].[Name] = @QueryString
		)
		BEGIN	
					 
			EXEC @QueryStringId = [dbo].[usp_AddQueryString] 
			@QueryString = @QueryString
					 
		END	

	END

	IF (@QueryStringId = -1)
	BEGIN

		SELECT  @QueryStringId = [ROWID] 
		FROM  [dbo].[vwWebQueryStrings]
		WHERE [vwWebQueryStrings].[Name] = @QueryString

	END

	RETURN @QueryStringId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSessionId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_GetSessionId]
@Session						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SessionId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwWebSessions]
			WHERE [vwWebSessions].[Name] = @Session
		)
		BEGIN	
					 
			EXEC @SessionId = [dbo].[usp_AddSession] 
			@Session = @Session
					 
		END	

	END

	IF (@SessionId = -1)
	BEGIN

		SELECT  @SessionId = [ROWID] 
		FROM  [dbo].[vwWebSessions]
		WHERE [vwWebSessions].[Name] = @Session

	END

	RETURN @SessionId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetStackTraceId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetStackTraceId]
@StackTrace					varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @StackTraceId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwStackTraces]
			WHERE [vwStackTraces].[Value] = @StackTrace
		)
		BEGIN	
					 
			EXEC @StackTraceId = [dbo].[usp_AddStackTrace] 
			@StackTrace = @StackTrace
					 
		END	

	END

	IF (@StackTraceId = -1)
	BEGIN

		SELECT  @StackTraceId = [ROWID] 
		FROM  [dbo].[vwStackTraces]
		WHERE [vwStackTraces].[Value] = @StackTrace

	END

	RETURN @StackTraceId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSystemId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_GetSystemId]
@System						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SystemId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwSystems]
			WHERE [vwSystems].[Name] = @System
		)
		BEGIN	
					 
			EXEC @SystemId = [dbo].[usp_AddSystem] 
			@System = @System
					 
		END	

	END

	IF (@SystemId = -1)
	BEGIN

		SELECT  @SystemId = [ROWID] 
		FROM  [dbo].[vwSystems]
		WHERE [vwSystems].[Name] = @System

	END

	RETURN @SystemId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTechnologyId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTechnologyId]
@Technology					varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TechnologyId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwTechnologies]
			WHERE [vwTechnologies].[Name] = @Technology
		)
		BEGIN	
					 
			EXEC @TechnologyId = [dbo].[usp_AddTechnology] 
			@Technology = @Technology
					 
		END	

	END

	IF (@TechnologyId = -1)
	BEGIN

		SELECT  @TechnologyId = [ROWID] 
		FROM  [dbo].[vwTechnologies]
		WHERE [vwTechnologies].[Name] = @Technology

	END

	RETURN @TechnologyId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUrlId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetUrlId]
@URL						varchar(max),
@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @URLId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  [ROWID] 
			FROM  [dbo].[vwWebUrls]
			WHERE [vwWebUrls].[Name] = @URL
		)
		BEGIN	
					 
			EXEC @URLId = [dbo].[usp_AddURL] 
			@URL = @URL
					 
		END	

	END

	IF (@URLId = -1)
	BEGIN

		SELECT  @URLId = [ROWID] 
		FROM  [dbo].[vwWebUrls]
		WHERE [vwWebUrls].[Name] = @URL

	END

	RETURN @URLId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWebDataId]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetWebDataId]
@UrlId					int					= NULL,
@QueryStringId			int					= NULL,
@SessionId				int					= NULL,
@InsertIfNotFound		bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @WebDataId INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT [ROWID] 
			FROM  [dbo].[vwWebData]
			WHERE
			[vwWebData].[QueryStringId] = @QueryStringId AND
			[vwWebData].[UrlId] = @UrlId AND
			[vwWebData].[SessionId] = @SessionId
		)
		BEGIN						 
			EXEC @WebDataId = [dbo].[usp_AddWebData] 
			@UrlId = @UrlId,
			@QueryStringId = @QueryStringId,
			@SessionId = @SessionId
					 
		END	

	END

	IF (@WebDataId = -1)
	BEGIN

		SELECT  @WebDataId = [ROWID] 
		FROM  [dbo].[vwWebData]
		WHERE
		[vwWebData].[QueryStringId] = @QueryStringId AND
		[vwWebData].[UrlId] = @UrlId AND
		[vwWebData].[SessionId] = @SessionId

	END

	RETURN @WebDataId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWebErrorInformation]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetWebErrorInformation]
@WebDataId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	[Session]
      ,[URL]
      ,[QueryString]
  FROM [dbo].[vwWebErrorInformation]
  WHERE [ROWID] = @WebDataId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_LogError]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LogError]
@System				varchar(max)		= 'UNKNOWN',
@Technology 		varchar(max),
@ErrorType			varchar(max),
@ObjectThrownIn		varchar(max),
@ErrorMessage		varchar(max),
@MachineName		varchar(max)		= NULL,
@ActiveUser			varchar(max)		= NULL,
@MethodDataId		int					= NULL,
@WebDataId			int					= NULL,
@SqlDataId			int					= NULL
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION LogBasicErrorData
		
		DECLARE @SystemId int = -1;
		DECLARE @TechnologyId int = -1;
		DECLARE @ErrorTypeId int = -1;
		DECLARE @ObjectThrownInId int = -1;
		DECLARE @ErrorMessageId int = -1;
		DECLARE @MachineNameId int = NULL;
		DECLARE @ActiveUserId int = NULL;
		
	
		EXEC	@SystemId = [dbo].[usp_GetSystemId]
				@System = @System,
				@InsertIfNotFound = 1		
	
		EXEC	@TechnologyId = [dbo].[usp_GetTechnologyId]
				@Technology = @Technology,
				@InsertIfNotFound = 1
	
		EXEC	@ErrorTypeId = [dbo].[usp_GetErrorTypeId]
				@ErrorType = @ErrorType,
				@InsertIfNotFound = 1

	
		EXEC	@ObjectThrownInId = [dbo].[usp_GetObjectThrownInId]
				@ObjectThrownIn = @ObjectThrownIn,
				@InsertIfNotFound = 1				

	
		EXEC	@ErrorMessageId = [dbo].[usp_GetErrorMessageId]
				@ErrorMessage = @ErrorMessage,
				@InsertIfNotFound = 1			

	
		if(@MachineName IS NOT NULL)
		BEGIN		
			EXEC	@MachineNameId = [dbo].[usp_GetMachineNameId]
					@MachineName = @MachineName,
					@InsertIfNotFound = 1		
		END	

	
		if(@ActiveUser IS NOT NULL)
		BEGIN		
			EXEC	@ActiveUserId = [dbo].[usp_GetActiveUserId]
					@ActiveUser = @ActiveUser,
					@InsertIfNotFound = 1
		END	

		INSERT INTO [dbo].[vwErrors]
           ([SystemId]
		   ,[TechnologyId]
           ,[ErrorTypeId]
           ,[ObjectThrownInId]
           ,[ErrorMessageId]
           ,[MachineNameId]
           ,[ActiveUserId]
           ,[MethodDataId]
           ,[WebDataId]
           ,[SqlDataId]
           ,[DateTimeAdded])
		 VALUES
			(@SystemId
			,@TechnologyId
			,@ErrorTypeId
			,@ObjectThrownInId
			,@ErrorMessageId
			,@MachineNameId
			,@ActiveUserId
			,@MethodDataId
			,@WebDataId
			,@SqlDataId
			,GETDATE())

		COMMIT TRANSACTION LogBasicErrorData

		SELECT SCOPE_IDENTITY() as 'ErrorId';
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION LogBasicErrorData;
		THROW;

	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_LogError_MethodData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LogError_MethodData]
@Class				varchar(max)		= NULL,
@Package			varchar(max)		= NULL,
@MethodParameters	varchar(max)		= NULL,
@StackTrace			varchar(max)		= NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

	BEGIN TRANSACTION LogMethodErrorData
		
		DECLARE @ClassId int = NULL;
		DECLARE @PackageId int = NULL;
		DECLARE @MethodParametersId int = NULL;
		DECLARE @StackTraceId int = NULL;

	
		IF(@Class IS NOT NULL)
		BEGIN	
			EXEC	@ClassId = [dbo].[usp_GetClassId]
					@Class = @Class,
					@InsertIfNotFound = 1
		END
				
		IF(@Package IS NOT NULL)
		BEGIN	
			EXEC	@PackageId = [dbo].[usp_GetPackageId]
					@Package = @Package,
					@InsertIfNotFound = 1
		END

		IF(@MethodParameters IS NOT NULL)
		BEGIN		
			EXEC	@MethodParametersId = [dbo].[usp_GetMethodParametersId]
					@MethodParameters = @MethodParameters,
					@InsertIfNotFound = 1
		END	

		IF(@StackTrace IS NOT NULL)
		BEGIN	
			EXEC	@StackTraceId = [dbo].[usp_GetStackTraceId]
					@StackTrace = @StackTrace,
					@InsertIfNotFound = 1
		END		
			
		DECLARE @MethodDataId	int = -1;
		
		EXEC	@MethodDataId = [dbo].[usp_GetMethodDataId]
				@ClassId = @ClassId,
				@PackageId = @PackageId,
				@MethodParametersId =@MethodParametersId,
				@StackTraceId = @StackTraceId,
				@InsertIfNotFound =1


		COMMIT TRANSACTION LogMethodErrorData
	
		SELECT @MethodDataId as 'MethodDataId';
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION LogMethodErrorData;

		THROW;

	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_LogError_SqlData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LogError_SqlData]
@Database		varchar(200),
@User			varchar(200),
@Severity		varchar(200),
@LineNumber		int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 'Not Implemented'

	--INSERT INTO [ErrorArchive].[tbSqlInfoLinker]
 --          ([ErrorId]
 --          ,[DatabaseId]
 --          ,[UserId]
 --          ,[Severity]
 --          ,[LineNumber])
 --    VALUES
 --          (<ErrorId, bigint,>
 --          ,<DatabaseId, int,>
 --          ,<UserId, int,>
 --          ,<Severity, int,>
 --          ,<LineNumber, int,>)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_LogError_WebData]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LogError_WebData]
@URL			varchar(max),
@QueryString	varchar(max),
@Session		varchar(max)

AS
BEGIN
	SET NOCOUNT ON;
		BEGIN TRY

	BEGIN TRANSACTION LogWebErrorData
		
		DECLARE @UrlId int = NULL;
		DECLARE @QueryStringId int = NULL;
		DECLARE @SessionId int = NULL;

	
		IF(@URL IS NOT NULL)
		BEGIN	
			EXEC	@UrlId = [dbo].[usp_GetUrlId]
					@URL = @URL,
					@InsertIfNotFound = 1
		END
			
		IF(@QueryString IS NOT NULL)
		BEGIN	
			EXEC	@QueryStringId = [dbo].[usp_GetQueryStringId]
					@QueryString = @QueryString,
					@InsertIfNotFound = 1
		END
		
		IF(@Session IS NOT NULL)
		BEGIN		
			EXEC	@SessionId = [dbo].[usp_GetSessionId]
					@Session = @Session,
					@InsertIfNotFound = 1
		END	
					
		DECLARE @WebDataId	int = -1;
		
		EXEC	@WebDataId = [dbo].[usp_GetWebDataId]
				@UrlId = @UrlId,
				@QueryStringId = @QueryStringId,
				@SessionId =@SessionId,
				@InsertIfNotFound =1
				
		COMMIT TRANSACTION LogWebErrorData
	
		SELECT @WebDataId as [WebErrorId];
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION LogWebErrorData;

		THROW;

	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchBasicErrors]    Script Date: 05/09/2018 19:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SearchBasicErrors]
@System			varchar(max) = NULL,
@Technology		varchar(max) = NULL,
@ErrorType		varchar(max) = NULL,
@ObjectThrownIn varchar(max) = NULL,
@MachineName	varchar(max) = NULL,
@ActiveUser		varchar(max) = NULL,
@ErrorMessage	varchar(max) = NULL,
@MinDateTime	datetime	 = NULL,
@MaxDateTime	datetime	 = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF(@MinDateTime IS NULL)
	BEGIN
		 SET @MinDateTime = dateadd(DAY,-30, datediff(day,0, getdate()));
	END

	SELECT 
	BasicErrorData.ROWID as ErrorId,
	BasicErrorData.[System] as System,
	BasicErrorData.Technology as Technology,
	BasicErrorData.ErrorType,
	BasicErrorData.ObjectThrownIn,
	BasicErrorData.MachineName,
	BasicErrorData.ActiveUser,
	BasicErrorData.ErrorMessage,
	BasicErrorData.MethodDataId,
	BasicErrorData.WebDataId,
	BasicErrorData.SqlDataId,
	BasicErrorData.DateTimeAdded

	FROM 
	fn_SearchBasicErrors(@System,@Technology,@ErrorType,@ObjectThrownIn,@MachineName,@ActiveUser,@ErrorMessage,@MinDateTime,@MaxDateTime) AS BasicErrorData

	ORDER BY [BasicErrorData].ROWID DESC
	
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbActiveUsers"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwActiveUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwActiveUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[15] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwErrors"
            Begin Extent = 
               Top = 70
               Left = 573
               Bottom = 296
               Right = 758
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwTechnologies"
            Begin Extent = 
               Top = 10
               Left = 128
               Bottom = 106
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwErrorTypes"
            Begin Extent = 
               Top = 0
               Left = 1030
               Bottom = 96
               Right = 1200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwObjectThrownIn"
            Begin Extent = 
               Top = 106
               Left = 1024
               Bottom = 202
               Right = 1194
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwErrorMessages"
            Begin Extent = 
               Top = 113
               Left = 21
               Bottom = 209
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwMachineNames"
            Begin Extent = 
               Top = 236
               Left = 861
               Bottom = 332
               Right = 1031
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwActiveUsers"
            Begin Extent = 
               Top = 213
               Left = 87
               Bottom = 309
               Right = 257
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwBasicErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwSystems"
            Begin Extent = 
               Top = 4
               Left = 368
               Bottom = 100
               Right = 538
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1740
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwBasicErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwBasicErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbClasses"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwClasses'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwClasses'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbErrorMessages"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrorMessages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrorMessages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbErrors"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbErrorTypes"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrorTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwErrorTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbMachineNames"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMachineNames'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMachineNames'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbMethodData"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwMethodData"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 247
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwClasses"
            Begin Extent = 
               Top = 85
               Left = 689
               Bottom = 181
               Right = 859
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwMethodParameters"
            Begin Extent = 
               Top = 166
               Left = 491
               Bottom = 262
               Right = 661
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwPackages"
            Begin Extent = 
               Top = 16
               Left = 501
               Bottom = 112
               Right = 671
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwStackTraces"
            Begin Extent = 
               Top = 195
               Left = 297
               Bottom = 291
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1740
         Table = 1905
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbMethodParameters"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodParameters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwMethodParameters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbObjectThrownIn"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwObjectThrownIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwObjectThrownIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbPackages"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPackages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPackages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbSqlData"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSqlData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSqlData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbSqlDatabases"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSqlDatabases'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSqlDatabases'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbStackTraces"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwStackTraces'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwStackTraces'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbSystems"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSystems'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwSystems'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbTechnologies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwTechnologies'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwTechnologies'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbWebData"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwWebData"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwWebQueryStrings"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 102
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwWebUrls"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 102
               Right = 832
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vwWebSessions"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 102
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1830
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebErrorInformation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbWebQueryStrings"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebQueryStrings'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebQueryStrings'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbWebSessions"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebSessions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebSessions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbWebUrls"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebUrls'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwWebUrls'
GO
