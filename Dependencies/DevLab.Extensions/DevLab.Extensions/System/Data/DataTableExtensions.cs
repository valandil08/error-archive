﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace System.Data
{
    public static class DataTableExtensions
    {
        public static List<SelectListItem> ConvertToSelectItemList(this DataTable dt, string textColName, string valueColName)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = row[textColName].ToString();
                    item.Value = row[valueColName].ToString();

                    list.Add(item);
                }
            }

            return list;
        }
        
        public static string ConvertToJson(this DataTable dt)
        {
            if (dt == null)
            {
                return "";
            }


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }

            return serializer.Serialize(rows);
        }

        public static JsonResult ConvertToJsonResult(this DataTable dt)
        {

            string json = ConvertToJson(dt);

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = json;
            result.ContentEncoding = Encoding.UTF8;
            result.ContentType = "JSON";

            return result;
        }

    #region Row to Object Mapping Methods

        public static List<T> ToList<T>(this DataTable dt, Func<DataRow, T> selector) where T : new()
        {
            List<T> list = new List<T>();

            EnumerableRowCollection<T> target = dt.AsEnumerable().Select(selector);

            foreach (T row in target)
            {
                list.Add(row);
            }

            return list;
        }

        public static T ConvertRowToObject<T>(this DataTable dt, int rowNum, Func<DataRow, T> selector) where T : new()
        {
            if (dt.Rows.Count <= rowNum)
            {
                return default(T);
            }

            List<T> list = dt.Rows[rowNum].Table.ToList(selector);

            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }

        public static T ConvertFirstRowToObject<T>(this DataTable dt, Func<DataRow, T> selector) where T : new()
        {
            return ConvertRowToObject(dt, 0, selector);
        }

    #endregion

    }
}