﻿namespace System.Web.Mvc
{
    public static class TempDataExtensions
    {

        /// <summary>
        /// return true if the value stored in TempData under the key given is either "true" or "1"<br/>
        /// <p>if the key is invalid or there is not data stored under the key given it will return false</p>
        /// </summary>
        public static bool IsTrue(this TempDataDictionary tempData, string key)
        {
            if (tempData[key] == null)
            {
                return false;
            }

            string value = tempData[key].ToString().ToLower();

            return value.Equals("true") || value.Equals("1");
        }

        /// <summary>
        /// return true if the value stored in TempData under the key given is either "false" or "0"<br/>
        /// <p>if the key is invalid or there is not data stored under the key given it will return false</p>
        /// </summary>
        public static bool IsNotTrue(this TempDataDictionary tempData, string key)
        {
            if (tempData[key] == null)
            {
                return false;
            }

            string value = tempData[key].ToString().ToLower();

            return value.Equals("false") || value.Equals("0");
        }

    }
}