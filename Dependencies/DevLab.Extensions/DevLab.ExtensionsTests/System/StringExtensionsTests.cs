﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Tests
{
    [TestClass()]
    public class StringExtensionsTests
    {
        [TestMethod()]
        public void ToByteNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToSByteNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToShortNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToUShortNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToIntNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToUIntNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToLongNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToULongNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToFloatNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToDoubleNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToCharNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToDecimalNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToBoolNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToDateTimeNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToTimeSpanNullTest()
        {
            Assert.Fail();
        }
    }
}