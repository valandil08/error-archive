﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Tests
{
    [TestClass()]
    public class ObjectExtensionsTests
    {
        [TestMethod()]
        public void ToStringNullTest()
        {
            string test = null;

            if (test.ToStringNull() != null)
            {
                Assert.Fail();
            }

            test = "";

            if (test.ToStringNull() == null)
            {
                Assert.Fail();
            }

            test = "test";

            if (test.ToStringNull() == null)
            {
                Assert.Fail();
            }
        }
    }
}