﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace System.Web.Mvc.Tests
{
    [TestClass()]
    public class TempDataExtensionsTests
    {
        private TempDataDictionary tempData = GenerateTempData();

        [TestMethod()]
        public void IsTrueTest()
        {
            if (tempData.IsTrue("true1") == false)
            {
                Assert.Fail();
            }

            if (tempData.IsTrue("true2") == false)
            {
                Assert.Fail();
            }

            if (tempData.IsTrue("false1") == true)
            {
                Assert.Fail();
            }

            if (tempData.IsTrue("false2") == true)
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void IsNotTrueTest()
        {
            if (tempData.IsNotTrue("true1") == true)
            {
                Assert.Fail();
            }

            if (tempData.IsNotTrue("true2") == true)
            {
                Assert.Fail();
            }

            if (tempData.IsNotTrue("false1") == false)
            {
                Assert.Fail();
            }

            if (tempData.IsNotTrue("false2") == false)
            {
                Assert.Fail();
            }
        }

        private static TempDataDictionary GenerateTempData()
        {
            TempDataDictionary tempData = new TempDataDictionary();

            tempData["true1"] = true;
            tempData["true2"] = "true";

            tempData["false1"] = false;
            tempData["false2"] = "false";

            return tempData;
        }
    }
}