﻿using DevLab.Core.Connections;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;

namespace DevLab.Core
{
    public class AppSetup
    {
        private static bool useDbConfig = (ConfigurationManager.ConnectionStrings["AppConfigs"] != null);

        public static void AppStart()
        {
            NameValueCollection applicationSettings = ConfigurationManager.GetSection("appSettings") as NameValueCollection;

            if (applicationSettings != null)
            {
                foreach (string key in applicationSettings.AllKeys)
                {
                    AppConfig.SetAppSetting(key, applicationSettings[key]);
                }
            }

            ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;

            if (connectionStrings != null)
            {
                foreach (ConnectionStringSettings connectionString in connectionStrings)
                {
                    AppConfig.SetConnectionString(connectionString.Name, connectionString.ConnectionString);
                }
            }

            if (AppConfig.GetAppSetting("AppName") == null)
            {
                AppConfig.SetAppSetting("AppName", "Unknown Application");
            }
        }
    }
}
