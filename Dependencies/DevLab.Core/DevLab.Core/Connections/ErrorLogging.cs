﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DevLab.Core.Connections
{
    public class ErrorLogging
    {
        private enum ErrorType { WPF, Website, WinForms };

        public static void LogWebAppError(Exception ex, string pageURL = null, string username = null)
        {
            // don't log LoggedException's
            if (ex.GetType().ToString().Equals("LoggedException"))
            {
                return;
            }

            string session = "";
            if (HttpContext.Current.Session != null)
            {
                foreach (string Key_loopVariable in HttpContext.Current.Session.Keys)
                {
                    session += Key_loopVariable + "=" + HttpContext.Current.Session[Key_loopVariable].ToString() + "\n";
                }
            }

            string queryString = "";
            if (HttpContext.Current.Request.QueryString != null)
            {
                queryString = HttpContext.Current.Request.QueryString.ToString();
            }

            LogErrorInSqlDatabase(ex, pageURL, session, queryString, username);
        }

        private static void LogErrorInSqlDatabase(Exception ex, string pageURL = null, string session = null, string queryString = null, string username = null)
        {
            ErrorData errorData = new ErrorData();
        
            errorData.DateTimeErrorThrown = DateTime.Now;
            errorData.ErrorType = ex.GetType().ToString();
            errorData.Stacktrace = ex.StackTrace;

            GetMethodData(ref errorData, ref ex);
        }

        private static void GetMethodData(ref ErrorData errorData, ref Exception ex)
        {
            // extract the stacktrace
            StackTrace st = new StackTrace(ex, true);

            // extract the frame the error was thrown on
            StackFrame frame = st.GetFrame(0);

            // extract and store information about the method with the error
            MethodBase methodData = frame.GetMethod();

            // cast into its base class
            MethodInfo methodInfo = (MethodInfo)methodData;

            errorData.MethodName = methodInfo.Name;
            errorData.MethodReturnType = methodInfo.ReturnType.Name;
            errorData.ClassName = methodInfo.ReflectedType.Name;
            errorData.IsConstructor = methodInfo.IsConstructor;
            errorData.IsVirtual = methodInfo.IsVirtual;
            errorData.IsStatic = methodInfo.IsStatic;
            errorData.IsFinal = methodInfo.IsFinal;

            errorData.MethodAccessModifier = "protected";

            if (methodInfo.IsPrivate)
            {
                errorData.MethodAccessModifier = "private";
            }
            else if (methodInfo.IsPublic)
            {
                errorData.MethodAccessModifier = "public";
            }

            GetParameterInfo(ref errorData, ref ex, methodData.GetParameters());

            // extract generic information about the error
            errorData.LineNumber = frame.GetFileLineNumber();
        }

        private static void GetParameterInfo(ref ErrorData errorData, ref Exception ex, ParameterInfo[] parameterInfo)
        {
            string parameters = "";

            if (parameterInfo != null && parameterInfo.Length > 0)
            {
                bool first = true;
                foreach (ParameterInfo param in parameterInfo)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        parameters += ", ";
                    }

                    string parameterType = param.ParameterType.Name.Trim();

                    switch (parameterType)
                    {
                        case "String":
                            parameters += "string";
                            break;

                        case "Double":
                            parameters += "double";
                            break;

                        case "Byte":
                            parameters += "byte";
                            break;

                        case "Single":
                            parameters += "float";
                            break;

                        case "Int16":
                            parameters += "short";
                            break;

                        case "Int32":
                            parameters += "int";
                            break;

                        case "Int64":
                            parameters += "long";
                            break;

                        default:
                            parameters += param.ParameterType.Name;
                            break;
                    }
                    parameters += " " + param.Name;
                }
            }

            errorData.MethodParameters = parameters;
        }


        public static void LogWebsiteError(){

        } 



        private class ErrorData
        {
            public DateTime DateTimeErrorThrown { get; set; }
            public string System { get; set; }
            public string ErrorType { get; set; }
            public string ErrorSource { get; set; }
            public string ErrorMessage { get; set; }
            public int LineNumber { get; set; }
            public string Username { get; set; }
            public string WebpageURL { get; set; }
            public string Session { get; set; }
            public string QueryString { get; set; }
            public string ClassName { get; set; }
            public string MethodName { get; set; }
            public string MethodParameters { get; set; }
            public string MethodAccessModifier { get; set; }
            public string MethodReturnType { get; set; }
            public string Stacktrace { get; set; }

            public bool IsStatic { get; set; }
            public bool IsFinal { get; set; }
            public bool IsVirtual { get; set; }
            public bool IsConstructor { get; set; }
        }
    }
}
