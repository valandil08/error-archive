﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DevLab.Core.Connections.SQL
{
    public class SqlServerDb : SqlDb, ISqlDb
    {
        public SqlServerDb(string conenctionStringName) : base(new SqlCommand(), conenctionStringName)
        {
        }

        protected override IDbConnection GetConnectionString(string connectionStringName)
        {
            return new SqlConnection(AppConfig.GetConnectionString(connectionStringName));
        }

        protected override DbDataAdapter GetDataAdapter()
        {
            return new SqlDataAdapter((SqlCommand)dbCommand);
        }
    }
}
