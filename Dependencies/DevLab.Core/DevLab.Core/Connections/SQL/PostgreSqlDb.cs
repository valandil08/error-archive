﻿using DevLab.Core.Connections.SQL;
using System;
using System.Data;
using Npgsql;
using System.Data.Common;

namespace DevLab.Core.Connections.SQL
{
    public class PostgreSqlDb : SqlDb, ISqlDb
    {
        public PostgreSqlDb(string conenctionStringName) : base(new NpgsqlCommand(), conenctionStringName)
        {
            // NpgsqlException
        }

        protected override IDbConnection GetConnectionString(string connectionStringName)
        {
            return new NpgsqlConnection(AppConfig.GetConnectionString(connectionStringName));
        }

        protected override DbDataAdapter GetDataAdapter()
        {
            return new NpgsqlDataAdapter((NpgsqlCommand)dbCommand);
        }
    }
}
