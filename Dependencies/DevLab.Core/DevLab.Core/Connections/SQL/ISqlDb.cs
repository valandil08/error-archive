﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DevLab.Core.Connections.SQL
{
    public interface ISqlDb
    {
        void SetSql(string sql);
        void SetStoredProcedure(string storedProcedureName);

        void AddParameter(string name, string value);
        void AddParameter<T>(string name, T value);

        void SetMaxReattempts(int numAttempts);
        void SetTimeout(int seconds);

        object RunScalarRequest();
        DataTable RunDataTableRequest();
        DataSet RunDataSetRequest();
    }
}
