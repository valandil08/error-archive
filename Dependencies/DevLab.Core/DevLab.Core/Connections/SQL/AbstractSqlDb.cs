﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DevLab.Core.Connections.SQL
{
    public abstract class SqlDb : ISqlDb
    {
        protected IDbCommand dbCommand;
        private int maxNumReattempts = 3;

        public SqlDb(IDbCommand dbCommand,string conenctionStringName)
        {
            this.dbCommand = dbCommand;
            dbCommand.Connection = GetConnectionString(conenctionStringName);
        }

        public void AddParameter(string name, string value)
        {
            IDbDataParameter parameter = dbCommand.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value;
            
            dbCommand.Parameters.Add(parameter);
        }

        public void AddParameter<T>(string name, T value)
        {
            IDbDataParameter parameter = dbCommand.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value;

            dbCommand.Parameters.Add(parameter);
        }

        public DataSet RunDataSetRequest()
        {
            OpenConnection(dbCommand.Connection);

            DataSet dataSet = new DataSet();

            int attemptCounter = 0;

            while (attemptCounter < maxNumReattempts)
            {
                try
                {
                    using (DbDataAdapter da = GetDataAdapter())
                    {
                        da.Fill(dataSet);
                        da.Dispose();
                    }

                    // make sure the loop dosen't repeat if sucessful
                    attemptCounter = maxNumReattempts;
                }
                catch (SqlException sqlEx)
                {
                    if (sqlEx.Number == 1205 && attemptCounter < maxNumReattempts)
                    {
                        attemptCounter++;
                    }
                    else
                    {
                        throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ") " + sqlEx.Message, sqlEx);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ")" + ex.Message, ex);
                }
                finally
                {
                    dbCommand.Connection.Close();
                }
            }

            return dataSet;
        }

        public DataTable RunDataTableRequest()
        {
            DataSet ds = RunDataSetRequest();

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
            }

            return null;
        }

        public object RunScalarRequest()
        {
            OpenConnection(dbCommand.Connection);

            object result = null;

            int attemptCounter = 0;

            while (attemptCounter <  maxNumReattempts)
            {
                try
                {
                    result = dbCommand.ExecuteScalar();

                    // make sure the loop dosen't repeat if sucessful
                    attemptCounter = maxNumReattempts;
                }
                catch (SqlException sqlEx)
                {
                    if (sqlEx.Number == 1205 && attemptCounter < maxNumReattempts)
                    {
                        attemptCounter++;
                    }
                    else
                    {
                        throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ") " + sqlEx.Message, sqlEx);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ")" + ex.Message, ex);
                }
                finally
                {
                    dbCommand.Connection.Close();
                }
            }

            return result;
        }
        
        public void SetMaxReattempts(int numAttempts)
        {
            maxNumReattempts = numAttempts;
        }

        public void SetSql(string sql)
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = sql;
        }

        public void SetStoredProcedure(string storedProcedureName)
        {
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.CommandText = storedProcedureName;
        }

        public void SetTimeout(int seconds)
        {
            dbCommand.CommandTimeout = seconds;
        }

        private void OpenConnection(IDbConnection connection)
        {
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message; // for debugging
                connection.Close();
                throw new Exception("(SqlDb) Error opening connection : " + connection.ConnectionString + "\n\r" + errorMessage, ex);
            }
        }

        protected abstract IDbConnection GetConnectionString(string connectionStringName);

        protected abstract DbDataAdapter GetDataAdapter();
    }
}
