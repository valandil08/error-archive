﻿using System;
namespace DevLab.Core
{
    public class LoggedException : Exception
    {
        public Exception exception { get; }

        public LoggedException(Exception exception)
        {
            if (exception.GetType().ToString().Equals("LoggedException"))
            {
                this.exception = ((LoggedException)exception).exception;
            }
            else
            {
                this.exception = exception;
            }
        }
    }
}
