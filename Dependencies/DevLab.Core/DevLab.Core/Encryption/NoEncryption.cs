﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab.Core.Encryption
{
    public class NoEncryption
    {
        public static string Encrypt(string text)
        {
            return text;
        }

        public static string Encrypt(string text, string encryptionKey)
        {
            return text;
        }

        public static string Encrypt(string text, string encryptionKey, byte[] salt)
        {
            return text;
        }

        public static string Decrypt(string text)
        {
            return text;
        }

        public static string Decrypt(string text, string encryptionKey)
        {
            return text;
        }

        public static string Decrypt(string text, string encryptionKey, byte[] salt)
        {
            return text;
        }
    }
}
