﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace DevLab.Core.Encryption
{
    public class NetHashingAlgorithm
    {
        public static string EncodePassword(string pass, string salt)
        {
            byte[] numArray;

            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] numArray2 = Convert.FromBase64String(salt);
            byte[] numArray3;

            // Hash password
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create(Membership.HashAlgorithmType);

            KeyedHashAlgorithm keyedHashAlgorithm = (KeyedHashAlgorithm)hashAlgorithm;

            if (keyedHashAlgorithm.Key.Length != numArray2.Length)
            {

                if (keyedHashAlgorithm.Key.Length >= numArray2.Length)
                {
                    numArray = new byte[keyedHashAlgorithm.Key.Length];
                    int num = 0;
                    while (true)
                    {
                        if (!(num < numArray.Length))
                        {
                            break;
                        }
                        int num1 = Math.Min(numArray2.Length, numArray.Length - num);
                        Buffer.BlockCopy(numArray2, 0, numArray, num, num1);
                        num = num + num1;
                    }
                    keyedHashAlgorithm.Key = numArray;
                }
                else
                {
                    numArray = new byte[keyedHashAlgorithm.Key.Length];
                    Buffer.BlockCopy(numArray2, 0, numArray, 0, numArray.Length);
                    keyedHashAlgorithm.Key = numArray;
                }
            }
            else
            {
                keyedHashAlgorithm.Key = numArray2;
            }
            numArray3 = keyedHashAlgorithm.ComputeHash(bytes);
            

            return Convert.ToBase64String(numArray3);      
        }
    }
}
