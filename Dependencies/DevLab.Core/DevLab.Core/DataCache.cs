﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace DevLab.Core
{
    public static class DataCache
    {
        private static List<DataContainer> cache = new List<DataContainer>();
        private static Semaphore sema = new Semaphore(1,1);

        public static void Set(string name, object value, int validForMinuites)
        {
            DateTime expiryDateTime = DateTime.Now.AddMilliseconds(validForMinuites);
            Set(name, value, expiryDateTime);
        }

        public static void Set(string name, object value, DateTime expiryDateTime)
        {

            bool found = false;

            foreach (DataContainer data in cache)
            {
                if (data.CheckName(name))
                {
                    data.SetValue(value, expiryDateTime);
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                cache.Add(new DataContainer(name, value, expiryDateTime));
            }
        }

        public static object Get(string name)
        {

            foreach (DataContainer data in cache)
            {
                if (data.CheckName(name))
                {
                    return data.GetValue();
                }
            }

            return null;
        }

        public class DataContainer
        {
            private string name;
            private object value;
            private DateTime expiryDate;

            private Semaphore sema = new Semaphore(1, 1);

            public DataContainer(string name, object value, DateTime expiryDate)
            {
                this.name = name;
                this.value = value;
                this.expiryDate = expiryDate;
            }

            public bool CheckName(string name)
            {
                return name.Equals(this.name);
            }

            public object GetValue()
            {
                object value = null;

                if (expiryDate >= DateTime.Now)
                {
                    value = this.value;
                }

                return value;
            }

            public void SetValue(object value, DateTime expiryDate)
            {
                this.value = value;
                this.expiryDate = expiryDate;
            }
        }
    }
}
