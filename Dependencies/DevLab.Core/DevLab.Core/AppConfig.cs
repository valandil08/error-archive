﻿using DevLab.Core.Connections;
using DevLab.Core.Encryption;
using System.Configuration;
using System.Data;
using System.Diagnostics;

namespace System
{
    public static class AppConfig
    {
        private static bool useEncryption = GetUseEncryptionSetting();

        private const string appSettingsPrefix = "AppSettings_";
        private const string connectionStringsPrefix = "ConnectionString_";

        public static bool DoesConnectionStringExist(string key)
        {
            if (DoesConnectionStringExistsInAppConfig(key))
            {
                return true;
            }

            key = FormatKey(key);

            return Environment.GetEnvironmentVariable(connectionStringsPrefix + key) != null;
        }

        public static string GetConnectionString(string key)
        {
            if (key == null || key.Trim().Equals(""))
            {
                return null;
            }

            if (DoesConnectionStringExistsInAppConfig(key))
            {
                return ConfigurationManager.ConnectionStrings[key].ConnectionString;
            }
            else
            {
                key = FormatKey(key);

                string encryptedValue = Environment.GetEnvironmentVariable(connectionStringsPrefix + key);

                if (encryptedValue == null)
                {
                    return null;
                }

                if (useEncryption)
                {
                    string encryptionKey = GetEncryptionKey(key);
                    return AES128Encryption.Decrypt(encryptedValue, encryptionKey);
                }
                else
                {
                    return encryptedValue;
                }
            }
        }
        
        public static void SetConnectionString(string key, string value)
        {
            // don't add if already exists in the app config
            if (DoesConnectionStringExistsInAppConfig(key) == false)
            {
                key = FormatKey(key);
                                
                if (useEncryption)
                {
                    string encryptionKey = GetEncryptionKey(key);
                    value = AES128Encryption.Encrypt(value, encryptionKey);
                }

                Environment.SetEnvironmentVariable(connectionStringsPrefix + key, value);

            }
        }
        
        
        public static bool DoesAppSettingExist(string key)
        {
            if (DoesAppSettingExistsInAppConfig(key))
            {
                return true;
            }

            key = FormatKey(key);

            return Environment.GetEnvironmentVariable(appSettingsPrefix + key) != null;
        }

        public static string GetAppSetting(string key)
        {
            if (key == null || key.Trim().Equals(""))
            {
                return null;
            }

            if (DoesConnectionStringExistsInAppConfig(key))
            {
                return ConfigurationManager.ConnectionStrings[key].ConnectionString;
            }
            else
            {
                key = FormatKey(key);
                
                string encryptedValue = Environment.GetEnvironmentVariable(appSettingsPrefix + key);

                if (encryptedValue == null)
                {
                    return null;
                }

                if (useEncryption)
                {
                    string encryptionKey = GetEncryptionKey(key);
                    return AES128Encryption.Decrypt(encryptedValue, encryptionKey);
                }
                else
                {
                    return encryptedValue;
                }
            }
        }

        public static void SetAppSetting(string key, string value)
        {
            if (DoesConnectionStringExistsInAppConfig(key) == false)
            {
                key = FormatKey(key);
                                
                if (useEncryption)
                {
                    string encryptionKey = GetEncryptionKey(key);
                    value = AES128Encryption.Encrypt(value, encryptionKey);
                }

                Environment.SetEnvironmentVariable(appSettingsPrefix + key, value);
            }
        }
        


        private static string FormatKey(string key)
        {
            if (key != null)
            {
                key = key.ToLower();
            }

            return key;
        }
        
        private static string GetEncryptionKey(string key)
        {
            return AES128Encryption.Encrypt(Environment.MachineName, key);
        }


        private static bool DoesAppSettingExistsInAppConfig(string name)
        {
            if(name == null)
            {
                return false;
            }

            if (ConfigurationManager.AppSettings[name] != null)
            {
                return true;
            }

            return false;
        }

        private static bool DoesConnectionStringExistsInAppConfig(string name)
        {
            if (name == null)
            {
                return false;
            }

            if (ConfigurationManager.ConnectionStrings[name] != null)
            {
                return true;
            }

            return false;
        }

        private static bool GetUseEncryptionSetting()
        {
            if (ConfigurationManager.ConnectionStrings["UseEncryption"] != null)
            {
                return ConfigurationManager.ConnectionStrings["UseEncryption"].ToString().ToLower().Equals("true");
            }

            return false;
        }
    }
}