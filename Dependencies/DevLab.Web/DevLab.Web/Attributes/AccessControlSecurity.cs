﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DevLab.Web.Attributes
{ 
    public class AccessControlSecurity : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // is true if passed forms authentication
            bool approved = base.AuthorizeCore(httpContext);

            // if has valid loggin
            if (approved)
            {
                string username = "username";

                string actionName = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
                string controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

                string url = "/" + controllerName + "/" + actionName;
                return true;
            }

            return approved;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // triggers a 401 error
            filterContext.Result = new RedirectToRouteResult(
            new RouteValueDictionary
            {
                { "action", "NoPermissions" },
                { "controller", "Error" }
            });
        }
    }
}