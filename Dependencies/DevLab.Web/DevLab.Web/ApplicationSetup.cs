﻿using DevLab.Core;
using DevLab.Core.Connections;
using DevLab.Web.BackgroundThreads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DevLab.Web
{
    public class ApplicationSetup
    {
        private static UserPageViewTracker userPageViewTracker;
        private static bool useUserPageViewTracker = AppConfig.DoesConnectionStringExist("WebAnalytics");

        public static void Application_Start()
        {
            AppSetup.AppStart();

            // remove all except the razor engine
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            if (useUserPageViewTracker) {
                userPageViewTracker = new UserPageViewTracker();
                userPageViewTracker.StartBackgroundThread();
            }
        }


        public static void Application_BeginRequest()
        {
            if (useUserPageViewTracker)
            {
                HttpContext.Current.Items["PageLoadStartTime"] = DateTime.Now;
            }
        }


        public static void Application_EndRequest()
        {
            if (useUserPageViewTracker)
            {
                DateTime pageLoadStart = (DateTime)HttpContext.Current.Items["PageLoadStartTime"];
                TimeSpan loadTime = DateTime.Now - pageLoadStart;

                AddToUserTracking(HttpContext.Current, loadTime);
            }
        }

        #region Application_EndRequest Methods

        private static void AddToUserTracking(HttpContext context, TimeSpan loadTimes)
        {
            string ntlogon = HttpContext.Current.User.Identity.Name;

            if (ntlogon.Equals(""))
            {
                ntlogon = "UNKNOWN USER";
            }

            if(context.Request.RequestContext.RouteData.Values["action"] != null)
            {
                string action = context.Request.RequestContext.RouteData.Values["action"].ToString();
                string controller = context.Request.RequestContext.RouteData.Values["controller"].ToString();
                if (IgnoreURL(controller, action) == false)
                {
                    string queryString = HttpContext.Current.Request.QueryString.ToString();

                    string browser = HttpContext.Current.Request.Browser.Browser;
                    string browserVersion = HttpContext.Current.Request.Browser.MajorVersion + "";
                    bool isBetaBrowser = HttpContext.Current.Request.Browser.Beta;
                    bool isMobileBrowser = HttpContext.Current.Request.Browser.IsMobileDevice;

                    userPageViewTracker.AddRecord(ntlogon, "/" + controller + "/" + action, queryString, browser, browserVersion, isMobileBrowser, isBetaBrowser, loadTimes.ToString());
                }
            }
        }

        private static bool IgnoreURL(string controller, string action){
            if(controller.ToLower().Contains("ajax"))
            {
                return true;
            }

            if (action.Length > 4)
            {
                if (action.Substring(0, 4).ToLower().Equals("ajax"))
                {
                    return true;
                }
            }

            string url = "/" + controller + "/" + action;
            string[] ignoreURLs = { "/__browserLink/requestData", "/bundles/Telerik", "/bundles/jquery", "/bundles/bootstrap", "/bundles/modernizr", "/Content/css" };

            foreach (string ignoreURL in ignoreURLs)
            {
                if (url.Contains(ignoreURL))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
        
        public static void Application_Error()
        {
            string url = HttpContext.Current.Request.RawUrl;

            // extract exception before new one is added
            Exception exception = HttpContext.Current.Server.GetLastError();
            
            ErrorLogging.LogWebAppError(exception, HttpContext.Current.Request.Url.ToString());   

            // redirect to relevant error page
            RedirectBasedOnErrorCode(exception);
        }
        
        private static void RedirectBasedOnErrorCode(Exception exception)
        {
            if (HttpContext.Current.Request.IsLocal == false)
            {
                HttpContext.Current.Response.Clear();

                HttpException httpException = exception as HttpException;

                if (httpException != null)
                {
                    string action;

                    // extract error code and get the page name to redirect them to
                    switch (httpException.GetHttpCode())
                    {
                        case 404:
                            // page not found
                            action = "PageNotFound";
                            break;

                        default:
                            action = "Error";
                            break;
                    }

                    // clear error on server
                    HttpContext.Current.Server.ClearError();

                    // redirect to relevant error page
                    HttpContext.Current.Response.Redirect(String.Format("~/Error/{0}", action, exception.Message));
                }
                else
                {
                    // if not http error redirect user to internal server error page
                    HttpContext.Current.Response.Redirect(String.Format("~/Error/{0}", "Error", exception.Message));
                }
            }
        }
        
        public static void RegisterRoutes(ref RouteCollection routes, string defaultControllerNamespace)
        {
            // loading views from sub-folders dosen't work
            AddControllerSubFolders(ref routes, defaultControllerNamespace);

            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        private static void AddControllerSubFolders(ref RouteCollection routes, string defaultControllerNamespace)
        {
            List<string> additionalRoutes = new List<string>();

            var controllers = Assembly.GetExecutingAssembly().GetExportedTypes().Where(t => typeof(ControllerBase).IsAssignableFrom(t)).Select(t => t);

            foreach (Type controller in controllers)
            {
                string subNamespace = controller.Namespace.Replace(defaultControllerNamespace, string.Empty);

                if (subNamespace.Equals("") == false)
                {
                    subNamespace = subNamespace.Substring(1);
                    if (additionalRoutes.Contains(subNamespace) == false)
                    {
                        additionalRoutes.Add(subNamespace);
                    }
                }
            }

            additionalRoutes.Sort();

            foreach (string route in additionalRoutes)
            {
                routes.MapRoute(
                    name: route,
                    url: route.Replace(".", "/") + "/{controller}/{action}/{id}",
                    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            }
        }
    }
}
