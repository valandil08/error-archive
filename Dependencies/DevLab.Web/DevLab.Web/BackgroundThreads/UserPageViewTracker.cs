﻿using DevLab.Core.Connections;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DevLab.Web.BackgroundThreads
{
    public class UserPageViewTracker
    {
        private Thread UserPageViewingDataThread;

        public ConcurrentQueue<UserTrackingData> userTrackingData = new ConcurrentQueue<UserTrackingData>();

        public void StartBackgroundThread()
        {
            UserPageViewingDataThread = new Thread(() => DataTransferThread());
            UserPageViewingDataThread.Start();
        }

        private void DataTransferThread()
        {
            int oneMinuiteDelay = 1000 * 60 * 1;
            while (!Environment.HasShutdownStarted)
            {
                // offload telemetry to sql server
                SendDataToSqlServer();

                Thread.Sleep(oneMinuiteDelay);
            }
        }

        private void SendDataToSqlServer()
        {
            int rowCount = userTrackingData.Count;

            if (rowCount > 0)
            {
                try
                {
                    List<UserTrackingData> data = new List<UserTrackingData>();
                    for (int i = 0; i < rowCount; i++)
                    {
                        UserTrackingData obj;
                        userTrackingData.TryDequeue(out obj);
                        data.Add(obj);
                    }
                    bool sucessfullyTranfered = false;

                    string sql = GenerateSqlQuery(data).Replace("\r\n", string.Empty);


                    while (sucessfullyTranfered == false)
                    {

                        // send data and detect if processed correctly
                        sucessfullyTranfered = false;// SqlServerDb.ExecuteScalarSQL("WebAnalytics", sql).ToString().Equals("1");



                        if (!sucessfullyTranfered)
                        {
                            // add delay before attempting to resend data
                            // to prevent self DDOS'ing
                            Thread.Sleep(500);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // extracted to not throw warning when compiling
                    string errorMessage = ex.Message;
                }

            }
        }

        private string GenerateSqlQuery(List<UserTrackingData> data)
        {
            string startOfQuery = @"BEGIN TRAN UpdateUserViewingHistory
                                                BEGIN TRY
                                                INSERT INTO [dbo].[UserPageViewingHistory]([NTLogon],[PageURL],[QueryString],[Browser],[BrowserVersion],[IsMobileBrowser],[IsBetaBrowser],[PageLoadTimes],[DateTime],[System])
                                                VALUES ";

            string endQuery = @" COMMIT TRAN UpdateUserViewingHistory
                                            SELECT '1' As [Sucess]
                                            END TRY
                                            BEGIN CATCH
                                            ROLLBACK TRAN UpdateUserViewingHistory;
                                            SELECT '0' As [Error]
                                            END CATCH";

            return startOfQuery + GenerateValuesSqlCode(data) + endQuery;
        }

        private string GenerateValuesSqlCode(List<UserTrackingData> data)
        {
            // store each row in stringbuilder
            StringBuilder sb = new StringBuilder();

            bool first = true;
            foreach (UserTrackingData element in data)
            {
                // only add commas after the first item
                if (!first)
                {
                    sb.Append(",");
                }
                else
                {
                    // makes all other rows have commas added before them
                    first = false;
                }
                //[NTLogon],[PageURL],[QueryString],[Browser],[BrowserVersion],[IsMobileBrowser],[IsBetaBrowser],[PageLoadTimes],[DateTime],[System]
                // add row to stringbuilder
                sb.Append("('" + element.NTLogon + "','" + element.PageURL + "','" + element.QueryString + "','" + element.Browser + "'," + element.BrowserVersion + "," + BoolToSqlBit(element.IsMobileBrowser) + "," + BoolToSqlBit(element.IsBetaBrowser) + ",'" + element.PageLoadTimes + "','" + element.datetime.Year+ "-" + element.datetime.Month + "-" + element.datetime.Day+" "+element.datetime.ToLongTimeString()+"','" + AppConfig.GetAppSetting("AppName") + "') ");
            }

            return sb.ToString();
        }

        private static int BoolToSqlBit(bool value)
        {
            if (value)
            {
                return 1;
            }

            return 0;
        }
        public void AddRecord(string ntlogon, string url, string queryString, string browser, string browserVersion, bool isMobileBrowser, bool isBetaBrowser, string loadTimes)
        {
            userTrackingData.Enqueue(new UserTrackingData(ntlogon, url, queryString, browser, browserVersion, isMobileBrowser, isBetaBrowser, loadTimes));
        }

        public class UserTrackingData
        {
            public string NTLogon;
            public string PageURL;
            public string QueryString;
            public string Browser;
            public string BrowserVersion;
            public bool IsMobileBrowser;
            public bool IsBetaBrowser;
            public string PageLoadTimes;
            public DateTime datetime;


            public UserTrackingData(string NTLogon, string PageURL, string QueryString, string Browser, string BrowserVersion, bool IsMobileBrowser, bool IsBetaBrowser, string PageLoadTimes)
            {
                datetime = DateTime.Now;
                this.NTLogon = NTLogon;
                this.PageURL = PageURL;
                this.QueryString = QueryString;
                this.Browser = Browser;
                this.BrowserVersion = BrowserVersion;
                this.IsMobileBrowser = IsMobileBrowser;
                this.IsBetaBrowser = IsBetaBrowser;
                this.PageLoadTimes = PageLoadTimes;
            }
        }
    }
}
