
If NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'ErrorArchive')
BEGIN

	CREATE DATABASE [ErrorArchive]
	 CONTAINMENT = NONE
	 ON  PRIMARY 
	( NAME = N'ErrorArchive', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ErrorArchive.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
	 LOG ON 
	( NAME = N'ErrorArchive_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ErrorArchive_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
	
END