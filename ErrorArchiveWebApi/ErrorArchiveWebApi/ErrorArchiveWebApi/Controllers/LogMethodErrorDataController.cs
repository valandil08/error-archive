﻿using DevLab.Core.Connections.SQL;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ErrorArchiveWebApi.Controllers
{
    public class LogMethodErrorDataController : ApiController
    {
        public string Post(string className, string package, string methodParameters, string stacktrace)
        {
            SqlServerDb db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("dbo.uspLogErrorMethodData");
            db.AddParameter("Class", className);
            db.AddParameter("Package", package);
            db.AddParameter("MethodParameters", methodParameters);
            db.AddParameter("Stacktrace", stacktrace);

            DataTable dt = db.RunDataTableRequest();

            string json = JsonConvert.SerializeObject(dt);

            return json;
        }
    }
}
