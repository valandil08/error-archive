﻿using DevLab.Core.Connections.SQL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ErrorArchiveWebApi.Controllers
{
    public class LogErrorDataController : ApiController
    {
        //[FromBody]
        public string Post(string system, string technology, string errorType, string objectThrownIn, string errorMessage,
                                       string machineName, string activeUser = null, int? methodDataId = null, int? webDataId = null, int? outerErrorId = null)
        {
            SqlServerDb db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("dbo.uspLogError");
            db.AddParameter("System", system);
            db.AddParameter("Technology", technology);
            db.AddParameter("ErrorType", errorType);
            db.AddParameter("ObjectThrownIn", objectThrownIn);
            db.AddParameter("ErrorMessage", errorMessage);
            db.AddParameter("MachineName", machineName);
            db.AddParameter("ActiveUser", activeUser);
            db.AddParameter("MethodDataId", methodDataId);
            db.AddParameter("WebDataId", webDataId);
            db.AddParameter("SqlDataId", null);

            if (outerErrorId != null)
            {
                db.AddParameter("OuterErrorId", outerErrorId);
            }

            DataTable dt = db.RunDataTableRequest();

            string json = JsonConvert.SerializeObject(dt);

            return json;
        }
    }
}