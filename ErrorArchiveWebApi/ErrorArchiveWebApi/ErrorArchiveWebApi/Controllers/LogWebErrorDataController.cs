﻿using DevLab.Core.Connections.SQL;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ErrorArchiveWebApi.Controllers
{
    public class LogWebErrorDataController : ApiController
    {
        public string Post(string url, string queryString, string session)
        {
            SqlServerDb db = new SqlServerDb("ErrorArchive");
            db.SetStoredProcedure("dbo.uspLogErrorWebData");
            db.AddParameter("URL", url);
            db.AddParameter("QueryString", queryString);
            db.AddParameter("Session", session);

            DataTable dt = db.RunDataTableRequest();

            string json = JsonConvert.SerializeObject(dt);

            return json;
        }


    }
}
